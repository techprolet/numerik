/**
 * "Numerik Klausur"
 * Linear splines functions
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */
function showLinearSplinesTheory(){
	$("#types").html("<p>" +
			"$k=1,\\quad r=1$" +
			"<br/>" +
			"Daten: $x_k,\\quad f(x_k) = y_k$" +
			"<br/>" +
			"$\\Rightarrow s_i(x)=\\left\\{\\begin{array}{ll}" +
			"y_i+m_i(x-x_i),	&	x\\in [x_i,x_{i+1}] \\\\" +
			"0,				&	x\\neq [x,x_{i+1}] \\\\" +
			"\\end{array}\\right.$" +
			"<br/>" +
			"$s_i (x_i) = y_i\\quad s_i (x_{i+1}) = s_{i+1} (x_{i+1})=y_{i+1}$" +
			"<br/>" +
			"$y_i + m_i (x_{i+1}-x_i)=y_{i+1}\\Rightarrow m_i=\\frac{y_{i+1}-y_i}{x_{i+1}-x_i}$" +
			"<br/>" +
			"$\\Rightarrow s_i = y_i +\\frac{y_{i+1}-y_i}{x_{i+1}-x_i}(x-x_i)$<br/>" +
			"oder" +
			"$s_i (x) = \\frac{x_{i+1}-x}{x_{i+1}-x_i}y_i + \\frac{x-x_i}{x_{i+1}x_i}y_{i+1}$" +
			"<br/>" +
			"Häufig werden Splines (für Programmierzwecke) auf einen Referenzintervall angegeben und bei Bedarf transformiert." +
			"<br/>" +
			"Transformation:<br/>" +
			"$t=\\frac{x-x_i}{x_{i+1}-x_i}\\Rightarrow [x_i,x_{i+1}]\\leftrightarrow [0,1] \\Rightarrow$<br/>" +
			"$s_i(x_i+th_i)=(1-t)y_i+ty_{i+1}$<br/>" +
			"$h_i=x_{i+1}-x_i$" +
			"</p>");
	retranslate();
}
function verboseLinearSplinesComputeForX(points, h, x){
	var verbose = "";
	verbose = "<b>Solve for x:</b><br/>";
	var i = 0;
	for (var k = 0;k<points.length-1;k++){
		if ((points[k].x<x)&&(points[k+1].x>x)){
			i=k;
			break;
		}
	}
	t = ((x-points[i].x)/(points[(i+1)].x-points[(i)].x));
	y =	((1-t)*(points[i].y)+(t*points[i+1].y));
	verbose+= "$t=\\frac{x-x_"+i+"}{x_"+(i+1)+"-x_"+i+"} = "
	+"\\frac{"+x+"-"+points[i].x+"}{"+points[(i+1)].x+"-"+(points[(i+1)].x<0?"(":"")+points[(i+1)].x+(points[(i+1)].x<0?")":"")+"}"
	+"=\\frac{"+(x-points[i].x)+"}{"+(points[(i+1)].x-points[(i)].x)+"} = "
	+t+" $<br/>";
	verbose+="$s_"+i+"(x_"+i+"+th_"+i+")=(1-t)y_"+i+"+ty_"+(i+1)+"\\Rightarrow $<br/>"
	+"$s_"+i+"("+points[i].x+"+"+t+"\\cdot"+h[i]+")= (1-"+t+")("+points[i].y+")+("+t+"\\cdot"+points[i+1].y+") = "
	+y+"$<br/>";
	$("#solution").append(verbose);

	return parseFloat(y.toFixed(5));
}


function verboseLinearSplines(points){
	var verbose = "";
	result = new Array();
	h = new Array();
	for (var i = 0;i<points.length-1;i++){
		h[i] = points[i+1].x-points[i].x;
		verbose+="$h_"+i+"=x_"+(i+1)+"-x_"+i+"="+points[i+1].x+"-("+points[i].x+")="+h[i]+"$<br/>";
		verbose+="$s_"+i+"(x_"+i+"+th_"+i+")=(1-t)y_"+i+"+ty_"+(i+1)+"\\Rightarrow s_"+i+"("+points[i].x+"+t"+h[i]+") = "
		+"(1-t)("+points[i].y+")+t("+points[i+1].y+")$<br/><br/>";
	}
	$("#solution").append(verbose);
	return h;
}

function getLinearSplines(){
	fillGivenArrays();
	showLinearSplinesTheory();
	$("#solution").html("");
	h = verboseLinearSplines(points);
	if ($("#x").val()!=""){
		$("#y").html(verboseLinearSplinesComputeForX(points,h, $("#x").val()));
	}

	retranslate();
};