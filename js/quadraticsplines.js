/**
 * "Numerik Klausur"
 * Copyright 2014 Pavlos Iliopoulos techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */

function showQuadraticSplinesTheory(){
	$("#types").html("<p>" +
			"<span class=\"fbox\">$k=2, r=1, s \\in S^{2,1}$</span><br/>" +
			"Um \glatte\ Kurven für die Approximation zu erhalten, wählen wir als Defekt $k=1$. Eine quadratische Spline-Funktion $s_i$ soll folgende Bedingungen erfüllen:" +
			"\\begin{displaymath}" +
			"\\begin{array}{llll}" +
			"s_i(x_i)     & & = y_i         & (Interpolationsbedingung) \\\\" +
			"s_i(x_{i+1}) &= s_{i+1}(x_{i+1})   & =y_{i+1}  & (Stetigkeitsbedingung 1) \\\\" +
			"s'_i(x_{i+1}) &= s'_{i+1}(x_{i+1}) & =m_{i+1}  & (Stetigkeitsbedingung 2)" +
			"\\end{array}" +
			"\\end{displaymath}" +
			"Die $s_i$ sind in $[x_i,x_{i+1}]$ Polynome zweiten Grades der Form" +
			"\\[" +
			"	s_i(x) = a_i + b_i (x-x_i) + c_i (x-x_i)^2" +
			"\\]" +
			"Daraus ergeben sich drei freie Koeffizienten pro Intervall. Zunächst haben wir aber vier (statt drei) Bedingungen, weswegen die Vorgabe der Ableitungen $y'_i,y'_{i+1}$ durch Parameter $m_i$ ersetzt wird, die so bestimmt werden, dass $s \\in S^{2,1}$ ist. Dazu setzen wir zunächst <br/>$a_i = y_i$ und<br/> $b_i = m_i$<br/> fest und betrachten dann die Ableitung von $s_i(x)$" +
			"$\\begin{array}\\\\" +
			"s_i(x) &= y_i + m_i (x-x_i) + c_i (x-x_i)^2 \\\\" +
			"s'_i(x) &= m_i + 2 c_i (x-x_i)" +
			"\\end{array}$<br/>" +
			"Aus Stetigkeitsbedingung 2 erhalten wir" +
			"$\\begin{array}\\\\" +
			"s'_i(x_{i+1}) &= m_i + 2 c_i (x_{i+1}-x_i) = m_{i+1} \\\\" +
			"s'_i(x_{i+1}) &= m_i + 2 c_i h_i = m_{i+1} \\\\" +
			"c_i &= \\frac{m_{i+1} - m_{i}}{2h_i}" +
			"\\end{array}$<br/>" +
			"Man kann nun sehen, dass es einen Zusammenhang zwischen $m_i$ und $m_{i+1}$ gibt. Um diesen zu Untersuchen, verwenden wir Stetigkeitsbedingung 1 und unsere Formel für die $c_i$:" +
			"$\\begin{array}\\\\" +
			"s_i(x_{i+1}) = y_i + m_i(x_{i+1}-x_i) + \\frac{m_{i+1} - m_{i}}{2h_i} (x_{i+1}-x_i)^2 &= y_{i+1} \\\\" +
			"	s_i(x_{i+1}) = y_i + m_i h_i + \\frac{m_{i+1} - m_{i}}{2h_i} h_i^2 &= y_{i+1} \\\\" +
			"m_i h_i + \\frac{m_{i+1} - m_{i}}{2} h_i &= y_{i+1} - y_i\\\\" +
			"m_i + \\frac{1}{2} (m_{i+1} - m_{i}) &= \\frac{y_{i+1} - y_i}{h_i} \\\\" +
			"m_{i+1} - m_{i} &= 2 \\frac{y_{i+1} - y_i}{h_i} - 2 m_i\\\\" +
			"m_{i+1} &= 2 \\frac{y_{i+1} - y_i}{h_i} - m_i\\\\" +
			"\\end{array}$<br/>" +
			"Nun kann noch die 1. Steigung $d_i = [x_i,x_{i+1}]$ substituiert werden:" +
			"$\\begin{array}\\\\" +
			"d_i &= [x_i,x_{i+1}] = \\frac{y_{i+1} - y_i}{h_i} \\\\" +
			"m_{i+1} &= 2 \\frac{y_{i+1} - y_i}{h_i} - m_i = 2d_i - m_i" +
			"\\end{array}$<br/>" +
			"Beziehungsweise nach Verschiebung der Indizes von $i+1$ nach $i$<br/>" +
			"<span class=\"fbox\">" +
			"$m_{i} = 2d_{i-1} - m_{i-1} $</span>$ =" +
			"2 \\frac{y_{i} - y_{i-1}}{h_{i-1}} - m_{i-1} =" +
			"2 \\frac{y_{i} - y_{i-1}}{x_i - x_{i-1}} - m_{i-1}" +
			"\\quad\\quad i=1,...,n-1" +
			"$<br/>" +
			"Für $n$ unbekannte Parameter $m_i$ haben wir somit $(n-1)$ Gleichungen erhalten. Um eine eindutige Lösung zu erhalten, können wir nun noch \\emph{einen Freiheitsgrad} belegen, indem wir $m_0$ wählen. Die restlichen $m_i$ ergeben sich aus der rekursiven Rechenvorschrift. Man könnte $m_0 = 0$ setzen, beliebig wählen oder z.B. so, dass man einen Spline mit möglichst kleiner Krümmung erhält." +
			"</p>");
	retranslate();
}

function getIntervals(points){
	var verbose = "<h3>Intervalle berechnen:</h3>";
	result = new Array();
	h = new Array();
	for (var i = 0;i<points.length-1;i++){
		h[i] = points[i+1].x-points[i].x;
		verbose+="$h_"+i+"=x_"+(i+1)+"-x_"+i+"="+points[i+1].x+"-("+points[i].x+")="+parseFloat(h[i].toFixed(5))+"$<br/>";
	}
	$("#solution").append(verbose);
	return h;
}
function getDifferentials(points,h){
	var verbose = "<h3>Steigungen berechnen:</h3>";
	result = new Array();
	d = new Array();
	for (var i = 0;i<points.length-1;i++){
		d[i] = (points[i+1].y-points[i].y)/(h[i]);
		verbose+="$d_"+i+"=\\frac{y_"+(i+1)+"-y_"+i+"}{x_"+(i+1)+"-x_"+i+"}=\\frac{"+points[i+1].y+"-("+points[i].y+")}{"+points[i+1].x+"-("+points[i].x+")}=\\frac{"+(points[i+1].y-points[i].y)+"}{"+h[i]+"}="+parseFloat(d[i].toFixed(5))+"$<br/>";
	}
	$("#solution").append(verbose);
	return d;
}

function getQuadraticSplinesWeights(points,d,m_0){

	var verbose = "<h3>$m_i$ für $m_0="+m_0+"$ berechnen:</h3>";
	m = new Array();
	m[0] = m_0;
	verbose+="$m_0="+m_0+"$<br/>";
	for (var i = 1;i<points.length;i++){
		m[i] = 2 *d[i-1]-m[i-1];
		verbose+="$m_"+i+"=2d_"+(i-1)+"-m_"+(i-1)+"=2\\cdot("+parseFloat(d[i-1].toFixed(5))+")-("+parseFloat(m[i-1].toFixed(5))+")="+parseFloat(m[i].toFixed(5))+"$<br/>";
	}
	$("#solution").append(verbose);

	return m;
}

function getQuadraticSplinesCs(points,h,d,m){
	var verbose = "<h3>$c_i$ für $m_0="+m[0]+"$ berechnen:</h3>";
	c = new Array();
	for (var i = 0;i<points.length-1;i++){
		c[i] = (m[i+1]-m[i])/(2*h[i]);
		verbose+="$c_"+i+"=\\frac{m_"+(i+1)+"- m_"+i+"}{2h_"+i+"}=\\frac{"+parseFloat(m[i+1].toFixed(5))+"-("+parseFloat(m[i].toFixed(5))+")}{2\\cdot("+h[i]+")}="+parseFloat(c[i].toFixed(5))+"$<br/>";
	}
	$("#solution").append(verbose);
	return c;
}

function verboseQuadraticSplines(points,d,m_0){
	var verbose = "<h2>Quadratische splines für $m_0="+m_0+"$</h2>";
	$("#solution").append(verbose);
	verbose = "";
	m = getQuadraticSplinesWeights(points,d,m_0);
	c = getQuadraticSplinesCs(points,h,d,m);
	
	verbose = "<h3>$s_i$ für $m_0="+m_0+"$</h3>";

	for (var i = 0;i<points.length-1;i++){
		verbose+="$s_"+i+"= a_"+i+" + b_"+i+"(x-x_"+i+")+c_"+i+"(x-x_"+i+")^2 =  "+points[i].y+"+("+parseFloat(m[i].toFixed(5))+")(x-("+points[i].x+"))+("+parseFloat(c[i].toFixed(5))+")(x-("+points[i].x+"))^2 $<br/>";
	}
	$("#solution").append(verbose);
	return c;
}

function getLeastKruemmung(points,h,d){
	var verbose = "<h2>Krümmung</h2>" +
			"<h3>$m_i$ für minimale Krümmung berechnen:</h3>";
	verbose +="<p>" +
			"\\begin{displaymath}" +
			"k(x) = \\frac{s''(x)}{(1+(s'(x))^2)^{\\frac{3}{2}}}\\approx s''(x)" +
			"\\end{displaymath}" +
			 "Bedingung: $\\int_{x_0}^{x_n}(s''(x))^2dx\\longrightarrow \\min$<br/>" +
			"$k(m_0)=\\int_{x_0}^{x_n}s''(x)dx = \\sum_{i=0}^{n-1}h_i(s_i'')^2=4\\sum_{i=0}^{n-1}\\frac{(d_i-m_i)^2}{h_i}$<br/>" +
			"Wir suchen: Eine explizite Formel für $m_i$ in Abhängigkeit von $m_0$" +
			"$a_0 = 0,\\quad a_i=d_{i-1}-a_{i-1},\\quad i=1,...,n-1$<br/>" +
			"$\\Rightarrow m_i = 2a_i + (-1)^im_0,\\quad i=0,1,...,n-1$<br/>" +
			"$\\Rightarrow k(m_0)=4\\sum_{i=0}^{n-1}\\frac{(d_i-2a_i-(-1)^i m_0)^2}{h_i}\\rightarrow \\min$" +
			"Wir lösen $k'(m_0)=0\\Rightarrow $" +
			"\\[m_0 = \\frac{\\sum_{i=0}^{n-1}\\frac{(-1)^i(d_i-2a_i)}{h_i}}{\\sum_{i=0}^{n-1}\\frac{1}{h_i}}\\]</p>";

	
	numerator = 0;
	denominator = 0;
	for (var i = 0;i<points.length-1;i++){
		
		numerator += (Math.pow(-1,i)*(d[i]-2*points[i].y))/h[i];
		console.log("Numerator:",numerator);
		denominator += 1/h[i];
		console.log("Denominator:",denominator);
	}
	var m_0 = numerator/denominator;
	m_0 = parseFloat(m_0.toFixed(5));
	
	verbose += "$m_0 = "+m_0+"$";
	
	$("#solution").append(verbose);
	return m_0;
	
}

function getQuadraticSplines(){
	fillGivenArrays();
	showQuadraticSplinesTheory();
	$("#solution").html("");
	h = getIntervals(points);
	d = getDifferentials(points,h);
	if ($("#m_0").val()!=""){
		verboseQuadraticSplines(points,d, parseFloat($("#m_0").val()));
	}

	m_0 = getLeastKruemmung(points, h, d);
	verboseQuadraticSplines(points,d, m_0);
	
	retranslate();
};
