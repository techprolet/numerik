/**
 * "Numerik Klausur"
 * Numerische Integration, Quadraturformel
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */


function showQuadraturformelTheory(){
	$("#types").html("" +
			"<h3>Quadraturformel</h3>" +
			"<p>" +
			"Gilt für eine Quadraturformel" +
			"\\[\\int_a^b f(x) dx \\approx Q_n = \\sum_{i=0}^n f(x_i) A_i,	\\]" +
			"dass $1,x,x^2,...,x^{k-1},x^k$ exakt integriert werden ($|\\delta|} = 0$), so heißt $k$ der \\emph{Exaktheitsgrad} der Quadraturformel.<br/>" +
			"</p>");
	retranslate();
}



function verboseQuadraturformel(x,y,a,b,n){
	var verbose = "<h2>Quadraturformel für $n="+n+"$</h2>";
	$("#solution").append(verbose);
	
	verbose = "";
	
	verbose+="$\\left." +
	"\\begin{array}{llllll}\\\\";
	for (var i = 0;i<n+1;i++){
		$("#qfx_"+i).html("$x_"+i+"="+x[i]+"$");
		$("#qfy_"+i).html("$y_"+i+"="+y[i]+"$");
		verbose+= "\\sum_{i=0}^{"+n+"}x_i^"+i+"A_i = " +
		"\\int_{"+a+"}^{"+b+"}x^"+i+ "dx = " +
		"\\int_{"+a+"}^{"+b+"}"+(i>0?("x"+(i>1?"^"+i:"")):"1")+ "dx = " +
				(i>0?("\\frac{("+b+")^{"+(i+1)+"}-("+a+")^"+(i+1)+"}{"+(i+1)+"}"):(b+"-("+a+")")) +
				"" +
				"\\\\\n";
	}
	verbose+="\\end{array}" +
			"\\right\\}\\Rightarrow" +
			"\\begin{array}{lllllllll}";
	
	var gleichungsMatrix = Array();
	
	for (var i = 0;i<n+1;i++){
		gleichungsMatrix[i] = Array();
		for (var k = 0; k<n+1;k++){
			gleichungsMatrix[i][k] = Math.pow(x[k],i);
			verbose+= "" + (k>0&&(x[k]>=0||i%2==0)?"+":"")+
					(i>0?parseFloat(Math.pow(x[k],i).toFixed(5)):"") +
					"a_"+k+"" +
					"&" +
					""
					;	
		}
		gleichungsMatrix[i][n+1] = (Math.pow(b,(i+1))-Math.pow(a,(i+1)))/(i+1);
		verbose+= "=" +
				"" +parseFloat(((Math.pow(b,(i+1))-Math.pow(a,(i+1)))/(i+1)).toFixed(5))+
						"\\\\\n";
	}
	verbose+="\\end{array}" +
			"\\right\\}" +
			"" +
			"" +
			"$";
	
	verbose += 	"" +
			"<br/>";
	$("#solution").append(verbose);
	a = verboseSolveMatrix(gleichungsMatrix, "a");
	
	verbose ="$Q_"+n+"(f) = ";
	var q = 0;
	for (var i = 0; i<n+1;i++){
		if ((i>0)&&(a[i]>=0)){
			verbose+= "+";
		}
		verbose+=parseFloat(a[i].toFixed(5))+"f("+parseFloat(x[i].toFixed(5))+")";
		console.log ("i",i,"y(i)",y[i]);
		q+= a[i]*y[i];
	}
	verbose += " = "+parseFloat(q.toFixed(5));

	$("#solution").append(verbose);	
}



function getQuadraturformel(){

	$(".rechteckregel").css("visibility","hidden");
	$(".quadraturformel").css("visibility","visible");
	$(".left-black").css("borderLeft","2px solid black");
	for (var i = 0;i<7;i++){
		$("#qfx_"+i+",#qfy_"+i).html("");
	}
	
	var n = parseInt($("#qf_n").val());
//	getValuesFromFunction($('#f').val(),(n+2),'a','b','x','y');
	var x;
	var a;
	var b;
	startVal = "a";
	endVal = "b";
	if ($("#"+startVal).val()!=""){
		a = parseFloat($("#"+startVal).val());
	} else {
		a = 0;
		$("#"+startVal).val(a);
	}
	if ($("#"+endVal).val()!=""){
		b = parseFloat ($("#"+endVal).val());
	} else {
		b = 1;
		$("#"+endVal).val(1);
	}

	x = Array();
	for (var i = 0; i<n+1;i++){
		if ($("#x_"+(i)).val ()!=""){
			x[i] = parseFloat($("#x_"+(i+1)).val ());
		} else {
			break;
		};
	}

	y = Array();
	for (var i = 0; i<n+1;i++){
		y[i] = parseFloat($("#y_"+(i+1)).val ());
	}
//	$("#n").val((n+2));
	
	h = (x[n]-x[0])/ (n);
	$("#h").html(parseFloat(h.toFixed(5)));
	
	showQuadraturformelTheory();

	verboseQuadraturformel(x, y,a,b, n);
	
	
	retranslate();
}