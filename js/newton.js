/**
 * "Numerik Klausur"
 * Newton interpolation method.
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */
function showNewtonTheory(){
	$("#types").html("<p>" +
			"\\[(L_nf)(x)=c_0+c_1(x-x_0)+c_2(x-x_0)(x-x_1)+...+c_n(x-x_0)(x-x_1)...(x-x_{n-1})\\]" +
			"mit $(L_nf)(x_i)=f(x_i)$<br/>\n" +
			"$(L_nf)(x_0)=f(x_0)=c_0$<br/>\n" +
			"$(L_nf)(x_1)= f(x_1)=c_0+c_1(x_1-x_0)$<br/>\n" +
			"...<br/>\n" +
			"$(L_nf)(x_n)=f(x_n)=c_0+c1(x_n-x_0)+...+c_{n-1}(x_n-x_0)...(x_n-x_{n-2})+c_{n}(x_n-x_0)...(x_n-x_{n-1})$<br/>\n" +
			"$\\left(\\begin{array}{lllll}" +
			"1 &		0 				&		0		& ... & 0  \\\\" +
			"1 & 	x_1-x_0 		&		0		&... & 0	\\\\" +
			"1 &		x_2-x_0			&	(x_2-x_0)(x_2-x_1)& 0\\\\" +
			".	&	.				&	.			&	.	&	.\\\\" +
			"1	& x_n-x_0			&...			&.		& (x_n-x_0)...(x_n-x_{n-1}\\\\" +
			"\\end{array}\\right) \\left(" +
			"\\begin{array}{l}" +
			"c_0\\\\" +
			"c_1\\\\" +
			".\\\\" +
			".\\\\" +
			"c_n" +
			"\\end{array}\\right)" +
			"=\\left(\\begin{array}{l}" +
			"f(x_0)\\\\" +
			"f(x_1)\\\\" +
			".\\\\" +
			".\\\\" +
			"f(x_n)\\\\" +
			"\\end{array}\\right)$<br/>\n" +
			"</p>" +
			"<p>" +
			"$ \\begin{array}{l}" +
			"x_0 & f(x_0) &                                 & \\\\" +
			"&        & \\frac{f(x_1)-f(x_0)}{x_1 - x_0}  & \\\\" +
			"x_1 & f(x_1) &                                 & \\frac{\\frac{f(x_2)-f(x_1)}{ x_2 - x_1}-\\frac{f(x_1)-f(x_0)}{ x_1 - x_0} }{ x_2 - x_0} \\\\" +
			"&        & \\frac{f(x_2)-f(x_1)}{ x_2 - x_1}  & \\\\" +
			"x_2 & f(x_2) &                                 & \\vdots \\\\" +
			"&        & \\vdots                          & \\\\" +
			"\\vdots &        &                                 & \\vdots \\\\" +
			"&        & \\vdots                          & \\\\" +
			"x_n & f(x_n) &                                 & \\\\" +
			"\\end{array}$ " +
			"</p>" +
			"<p class=\"text\">" +
			"<b>Vorteile:</b><br/>" +
			"• geringes Aufwand für Fkt. Wertberechnung<br/>" +
			"• leichte Hinzunahme weiterer Stützstellen <br/> " +
			"</p>");
	retranslate();
}

function verboseDifferences(points){
	var c = new Array();
	var verbose = "<table>"+
	"<tr><td>$x$</td><td>$p(x)$</td><td><p class=\"text\">Diff der Fktwerte</p></td>";
	for (var i = 0;i<points.length-2;i++){
		verbose += "<td></td>";
	}
	verbose +="</tr>";
	var diffs = new Array();
	for (var i = 0;i<points.length;i++){
		diffs[i] = new Array();
	}
	var level = 0;
	for (var i = 0;i<points.length;i++){
		diffs[0][i] = new Array();
		diffs[0][i].result = points[i].y;
		diffs[0][i].verbose = "$[x_"+i+"]="+points[i].y+"$";
//		if (i==points.length-1){
		if (i==0){
			c[0] = points[i].y;
			diffs[0][i].verbose += "<br/><b>(=$c_0$)</b>";
		}
		console.log ("Level 0:"+i+" "+diffs[0][i].result);
	}
	while (level<points.length-1){
		level++;
		for (var i = 0;i<points.length-level;i++){
			diffs[level][i] = new Array();
//			diffs[level][i].numerator;
//			diffs[level][i].denominator;
			diffs[level][i].result =parseFloat(( (diffs[level-1][i+1].result-diffs[level-1][i].result)/(points[i+level].x-points[i].x)).toFixed(5));
			diffs[level][i].verbose = "$[";
			for (var k=i;k<=i+level;k++){
				diffs[level][i].verbose += "x_"+k;
				if (k!=i+level){
					diffs[level][i].verbose += ",";
				}
			}
			diffs[level][i].verbose += "]="+diffs[level][i].result;
			diffs[level][i].verbose += "$";
//			diffs[level][i].result = (diffs[level-1][i+1].result-diffs[level-1][i].result);
//			if (i==points.length-level-1){
			if (i==0){	
				c[level] = diffs[level][i].result;
				diffs[level][i].verbose += "<br/><b>(=$c_"+level+"$)</b>";
			}
		}
		console.log ("Level "+level+":"+diffs[level]);
	}
	for (var i =0;i<2*points.length-1;i++){
		verbose +="<tr>";
		if (i%2 ==0){
			verbose +="<td>";
			verbose += points[i/2].x;
			verbose +="</td>";
		} else {
			verbose +="<td>";
			verbose +="</td>";
		}
		for (var k = 0; k <points.length;k++){
			verbose +="<td>";
			thisK = k;
			thisI = i;
			thisI = Math.floor((thisI-thisK)/2);
			if ((i-k)%2==1){
				thisI = -1;
			}
			if ((thisK<points.length)&&(thisK>=0)&&(thisI>=0)&&(thisI<points.length-thisK)){
				verbose += diffs[thisK][thisI].verbose;
			}
			verbose +="</td>";
		}
		verbose +="</tr>";
	}
	
	verbose +="</table>";
//	verbose +="<pre>";
//	verbose +=print_r(diffs);
//	verbose +="</pre>";
	$("#solution").append(verbose);
	return c;
}

function verboseNewton(points){
	var verbose = "";
	c = verboseDifferences(points);
	verbose += "$(L_"+(points.length-1)+"f)(x)=$<br/>";
	verbose += "$";
	for (var i = 0;i<points.length;i++){
		verbose += "c_"+i;
		for (var k=0;k<i;k++){
			verbose += "(x-x_"+k+")";
		}
		if (i<points.length-1){
			verbose += "+";
		}
	}
	verbose += "=$<br/>";
	console.log(c);
	verbose += "$";
	for (var i = 0;i<points.length;i++){
		if ((i>0)){
			verbose+="+";
		}
		verbose += parseFloat(c[i].toFixed(5));
		for (var k=0;k<i;k++){
			verbose += "(x"+(points[k].x>=0?"-":"+")+Math.abs(points[k].x)+")";
		}
	}
	verbose += "=$<br/>";
	
	verbose += "$";
	p = new Array(points.length)
	for (var i = 0;i<points.length;i++){
		if ((i>0)){
			verbose+="+";
		}
		verbose += parseFloat(c[i].toFixed(5));
		if (i>0){
			verbose += "(";
			if (i>1){
				var factors = new Array();
				for (var k=0;k<i;k++){
					factors.push(-points[k].x);
				}
				p[i] = getCoefficients(factors);
			} else {
				p[1] = [-points[0].x,1];
			}
			verbose += verbosePolynomial(p[i]);
			verbose += ")";
		} else {
			p[0]= [1];
		}
	}
	verbose += "=$<br/>";
	
	verbose += "$";
	for (var i = 0;i<points.length;i++){
		if ((i>0)){
			verbose+="+";
		}
		console.log ("i:",i,"c:",[c[i]],"p:",p[i]);
		p[i] = multiplyPolynomials([c[i]],p[i]);
		verbose += "("+verbosePolynomial(p[i])+")";
	}
	verbose += "=$<br/>";
	

	verbose += "$";
	result = [0];
	for (var i = 0;i<points.length;i++){
		result = addPolynomials (result, p[i]);
	}
	verbose += verbosePolynomial (result);
	verbose += "$<br/>";
	
	$("#solution").append(verbose);
	return result;
}

function getNewton(){
	fillGivenArrays();
	showNewtonTheory();
	$("#solution").html("");
	polynomial = verboseNewton(points);
	console.log(polynomial);
	if ($("#x").val()!=""){
		$("#y").html(computeForX (polynomial, $("#x").val()));
	}

	retranslate();
};