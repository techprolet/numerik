/**
 * "Numerik Klausur"
 * Lagrange interpolation method.
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */
function showLagrangeTheory(){
	$("#types").html("<p>" +
			"$w_i(x)= \\frac{(x-x_0)(x-x_1)\\cdots(x-x_{i-1})(x-x_{i+1})\\cdots(x-x_n)}{(x_i-x_0)(x_i-x_1)\\cdots(x_i-x_{i-1})(x_i-x_{i+1})\\cdots(x_i-x_n)}$<br/>" +
			"\\[(L_nf)(x)=\\sum_{i=0}^n f(x_i)w_i(x)\\]<br/>" +
			"</p>" +
			"<p class=\"text\">" +
			"<b>Vorteil:</b><br/>" +
			"• einfache Struktur<br/>" +
			"• Untersuchung von Rechnungsfehlern sehr einfach<br/>" +
			"• Koeffizienten sind explizit bekannt<br/>" +
			"• Abhängigkeit von (fehlervehafteten) Daten gut nachvollziehbar<br/>" +
			"<b>Nachteil:</b><br/>" +
			"• völlige Neuberechnung bei Hinzunahme einer Stützstelle nötig" +
			"</p>");
	retranslate();
}
function horner(array, x_scale, y_scale) {
   function recur(x, i, array) {
      if (i == 0) {
         return array[0];
      } else {
         return array[i] + x*recur(x, --i, array);
      }
   }
   return function(x) {
      return recur(x*x_scale, array.length-1, array)*y_scale;
   };
}



function denominator(i, points) {
   var result = 1;
   var x_i = points[i].x;
   for (var j=points.length; j--;) {
      if (i != j) {
        result *= x_i - points[j].x;
      }
   }
//    console.log(result);
   return result;
}

// calculate coefficients for Wi polynomial
function interpolation_polynomial(i, points) {
   var coefficients = zeros(points.length);
   $("#solution").append("Denominator " + i + ": " + denominator(i,points)+"<br/>");
   coefficients[0] = 1/denominator(i,points);
    console.log(coefficients[0]);
    //new Array(points.length);
   /*for (var s=points.length; s--;) {
      coefficients[s] = 1/denominator(i,points);
   }*/
   var new_coefficients;

   for (var k = 0; k<points.length; k++) {
      if (k == i) {
        continue;
      }
      new_coefficients = zeros(points.length);
       for (var j= (k < i) ? k+1 : k; j--;) {
         new_coefficients[j+1] += coefficients[j];
         new_coefficients[j] -= points[k].x*coefficients[j];
      }   
      coefficients = new_coefficients;
   }
   console.log(coefficients);
   return coefficients;
}

function verboseWi(i, points){
	var w = new Array();
   w.denominator = denominator(i,points);
//   $("#solution").append("Denominator " + i + ": " + denominator(i,points)+"<br/>");
   var verbose = "";
   verbose+="$w_"+i+" = \\frac{";
   //Zaehler
   for (var k = 0; k<points.length; k++) {
      if (k == i) {
        continue;
      }
      if (parseFloat(points[k].x)!=0){
    	  verbose+="(x"+(parseFloat(points[k].x)>0?"-":"+")+Math.abs(parseFloat(points[k].x))+")";
      } else {
    	  verbose+="x";
      }
   }
   verbose+="}{";
   //Nenner
   for (var k = 0; k<points.length; k++) {
	      if (k == i) {
	        continue;
	      }
	      verbose+="("+(parseFloat(points[i].x)!=0?points[i].x:"");
	      if (parseFloat(points[k].x)!=0){
	    	  verbose+=(parseFloat(points[k].x)>0?"-":"+")+Math.abs(parseFloat(points[k].x));
	      }
	      verbose+=")";
	   }
   verbose+="}";
   
   verbose+="=";
   verbose+=(parseFloat(w.denominator)<0?"-":"");
   verbose+="\\frac{1}{";
   verbose+=Math.abs(parseFloat(w.denominator));
   verbose+="}";
   var factors = new Array();
   for (var k = 0; k<points.length;k++){
	   if (i==k){
		   continue;
	   }
	   factors.push(-points[k].x);
   }
//   console.log ("Factors:"+factors);
   w.coefficients = getCoefficients (factors);
   verbose+="(";
   var nonZero = false;
   for (var k = points.length - 1;k>=0;k--){
	   if (w.coefficients[k]!=0){
		   if ((k<points.length-1)&&(nonZero)&&(w.coefficients[k]>=0)){
			   verbose+="+";
		   }
		   nonZero = true;
		    if (w.coefficients[k]==-1){
			   verbose+="-";
			   if (k==0){
				   verbose+="1";
			   }
		   } else if (w.coefficients[k]!=1){
			   verbose+=w.coefficients[k];
		   }
		   if (k>0){
			   verbose+="x";
			   if (k>1){
				   verbose+="^"+k;
			   }
		   }
	   }
   }
   verbose+=")";
	verbose+="$<br/>";
	$("#solution").append(verbose);
	return w;

}

//calculate coefficients of polynomial
function verboseLagrange(points) {
	var wi = new Array();
	for (var i=0; i<points.length; ++i) {
		wi[i] = verboseWi(i, points);
	}
	
	verbose = "<br/><br/>$(L_"+(points.length-1)+"f) = ";
	for (var i=0; i<points.length; ++i) {
		verbose+=(parseFloat(wi[i].denominator*points[i].y)<0?"-":"+");
		verbose+="\\frac{";
		verbose+=Math.abs(points[i].y);
		verbose+="}{";
		verbose+=Math.abs(parseFloat(wi[i].denominator));
		verbose+="}";
		verbose+="(";
		   var nonZero = false;
		   for (var k = points.length - 1;k>=0;k--){
			   if (wi[i].coefficients[k]!=0){
				   if ((k<points.length-1)&&(nonZero)&&(wi[i].coefficients[k]>=0)){
					   verbose+="+";
				   }
				   nonZero = true;
				    if (wi[i].coefficients[k]==-1){
					   verbose+="-";
					   if (k==0){
						   verbose+="1";
					   }
				   } else if (wi[i].coefficients[k]!=1){
					   verbose+=parseFloat(wi[i].coefficients[k].toFixed(5));
				   }
				   if (k>0){
					   verbose+="x";
					   if (k>1){
						   verbose+="^"+k;
					   }
				   }
			   }
		   }
		   verbose+=")";
	}
	verbose += "$<br/>$= ";
	for (var i=0; i<points.length; ++i) {
		var thisGcd = gcd(wi[i].denominator,points[i].y);
		wi[i].denominator = wi[i].denominator/thisGcd;
		points[i].y = points[i].y/thisGcd;
		verbose+=(parseFloat(wi[i].denominator*points[i].y)<0?"-":"+");
		verbose+="\\frac{";
		verbose+=Math.abs(points[i].y);
		verbose+="}{";
		verbose+=Math.abs(parseFloat(wi[i].denominator));
		verbose+="}";
		verbose+="(";
		   var nonZero = false;
		   for (var k = points.length - 1;k>=0;k--){
			   if (wi[i].coefficients[k]!=0){
				   if ((k<points.length-1)&&(nonZero)&&(wi[i].coefficients[k]>=0)){
					   verbose+="+";
				   }
				   nonZero = true;
				    if (wi[i].coefficients[k]==-1){
					   verbose+="-";
					   if (k==0){
						   verbose+="1";
					   }
				   } else if (wi[i].coefficients[k]!=1){
					   verbose+=parseFloat(wi[i].coefficients[k].toFixed(5));
				   }
				   if (k>0){
					   verbose+="x";
					   if (k>1){
						   verbose+="^"+k;
					   }
				   }
			   }
		   }
		   verbose+=")";
	}
	var commonDenominator = 1;
	for (var i=0; i<points.length; ++i) {
		commonDenominator = Math.abs(lcm(commonDenominator,wi[i].denominator));
	}
	verbose+="$<br/>$=\\frac{";

	for (var i=0; i<points.length; ++i) {
		verbose+=(parseFloat(wi[i].denominator)<0?"-":"+");
		wi[i].multiplier = points[i].y*commonDenominator/wi[i].denominator;
		verbose+=(Math.abs(wi[i].multiplier)!=1)?Math.abs(wi[i].multiplier):"";
		
		verbose+="(";
		   var nonZero = false;
		   for (var k = points.length - 1;k>=0;k--){
			   if (wi[i].coefficients[k]!=0){
				   if ((k<points.length-1)&&(nonZero)&&(wi[i].coefficients[k]>=0)){
					   verbose+="+";
				   }
				   nonZero = true;
				    if (wi[i].coefficients[k]==-1){
					   verbose+="-";
					   if (k==0){
						   verbose+="1";
					   }
				   } else if (wi[i].coefficients[k]!=1){
					   verbose+=parseFloat(wi[i].coefficients[k].toFixed(5));
				   }
				   if (k>0){
					   verbose+="x";
					   if (k>1){
						   verbose+="^"+k;
					   }
				   }
			   }
		   }
		   verbose+=")";
	}
	verbose+="}{"+commonDenominator+"}";
	
	
	var polynomial = zeros(points.length);
	for (var i=0; i<points.length; i++) {
		for (var k = 0;k<points.length;k++){
			polynomial[k]+=wi[i].multiplier*wi[i].coefficients[k];
		}
	}
	
//	console.log ("wiResult: "+wiResult);
	verbose+="$<br/>$=\\frac{";

	var nonZero = false;
	for (var k = points.length - 1;k>=0;k--){
		if (polynomial[k]!=0){
			if ((k<points.length-1)&&(nonZero)&&(polynomial[k]>=0)){
				verbose+="+";
			}
			nonZero = true;
		    if (polynomial[k]==-1){
		    	verbose+="-";
			   if (k==0){
				   verbose+="1";
			   }
		    } else if (polynomial[k]!=1){
		    	verbose+=parseFloat(polynomial[k].toFixed(5));
		    }
		    if (k>0){
		    	verbose+="x";
		    	if (k>1){
		    		verbose+="^"+k;
		    	}
		    }
		}
	}
	verbose+="}{"+commonDenominator+"}";
	

	verbose+="$<br/>$=";

	var nonZero = false;
	for (var k = points.length - 1;k>=0;k--){
		if (polynomial[k]!=0){
			polynomial[k]=polynomial[k]/commonDenominator;
			if ((k<points.length-1)&&(nonZero)&&(polynomial[k]>=0)){
				verbose+="+";
			}
			nonZero = true;
		    if (polynomial[k]==-1){
		    	verbose+="-";
			   if (k==0){
				   verbose+="1";
			   }
		    } else if (polynomial[k]!=1){
		    	verbose+=parseFloat(polynomial[k].toFixed(5));
		    } else if (k==0){
		    	verbose+=parseFloat(polynomial[k].toFixed(5));
		    }
		    if (k>0){
		    	verbose+="x";
		    	if (k>1){
		    		verbose+="^"+k;
		    	}
		    }
		}
	}
	
	
	verbose+="$<br/>";
	$("#solution").append(verbose);
	retranslate();
	return polynomial;
}

function getLagrange(){
	fillGivenArrays();
	showLagrangeTheory();
	$("#solution").html("");
	polynomial = verboseLagrange(points);
	console.log(polynomial);
	if ($("#x").val()!=""){
		$("#y").html(computeForX (polynomial, $("#x").val()));
	}
};