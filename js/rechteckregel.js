/**
 * "Numerik Klausur"
 * Numerische Integration, Rechteckregel
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */


function showRechteckRegelTheory(){
	$("#types").html("" +
			"<h3>Rechteckregel</h3>" +
			"<p>" +
			"$R_f =  \\sum_{i=1}^{n}f(\\xi_i)(x_i-x_{i-1})$<br/>" +
			"$a=x_0 < x_1 < .. < x_{n-1} < x_n=b$<br/>" +
			"Werten $\\xi_i = \\frac{x_{i-1}+x_i}{2}$<br/>" +
			"$\\int_a^b f(x) dx\\approx \\sum_{i=1}^n f(\\xi_i)(x_i-x_{i-1})$<br/>" +
			"Spezialfall: $x_i-x_{i-1} = h\\quad \\forall i \\Rightarrow $<br/>" +
			"$h = \\frac{b-a }{n}\\Rightarrow R_f = \\frac{b-a}{n}\\sum_{i=1}^nf(\\xi_i)$<br/>" +
			"$\\delta = \\int_{a}^bf(x)dx - \\sum_{i=1}n f(\\xi_i)\\frac{b-a}{n}$<br/>" +
			"\\underline{Fehler:}<br/>" +
			"\\[|\\delta| \\leq \\frac{M_2 (b-a)^3}{24n^2}\\]<br/>" +
			"$M_2 \\max_{x\\in [a,b]}|f''(x)|$<br/>" +
			"\\underline{Idee:} $f(x) = f(\\frac{x_i+x_{i+1}}{2})+f'(\\frac{x_i+x_{i+1}}{2}(x-x_{M,i})f''(\\xi_i )\\frac{)^2x-x_{M,i})^2}{2}\\qquad x\\in(x_i,x_{i+1})$<br/>" +
			"$\\int_{x_{i-1}}^{x_i}f(x)dx = f(x_{M,i})(x_i-x_{i-1})+ 0 + \\frac{1}{2}\\int_{x_i-1}^{x_i}f''(\\xi_i)(x-x_{M,i})^2dx$<br/>" +
			"$|\\delta_i| = |\\int_{x_i-1}^{x_i}f(x)dx - f(x_{M,i})(x_i-x_{i-1})| =" +
			"\\frac{1}{2}|\\int_{x_{i-1}}^{x_i}f''(\\xi_i)(x-x_{M,i}^2dx|\\leq \\frac{1}{2}\\int_{x_{i-1}}^{x_i}|f''(\\xi_i)|(x-x_{M,i})^2dx\\leq \\frac{1}{2}M_2\\int_{x_{i-1}}^{x_i}(x-x_{M,i})^2dx$<br/>" +
			"$|\\delta| = |\\int_{a}^bf(x)dx - \\sum_{i=1}^nf(\\xi_i)\\Delta x_i| = $<br/>" +
			"$|\\sum_{i=1}^n\\int_{x_{i-1}}^{x_i}f(x)dx - \\sum_{i=1}^nf(\\xi_i)\\Delta x_i|\\leq " +
			"\\sum_{i=1}^n |\\int_{x_{i-1}}^{x_i}f(x)dx- f(\\xi_i)\\Delta x_i | = $<br/>" +
			"$\\sum_{i=1}^{n}M_2 \\frac{(b-a)^3}{24n^3}=\\underbrace{M_2 \\frac{(b-a)^3}{24n^2}}_{global} M_2\\frac{h^3}{24} =\\underbrace{ M_2 \\frac{(b-a)^3}{24n^3} }_{lokal}$<br/>" +
			"</p>");
	retranslate();
}


function getXiFromX(xVals,xiId){
	xi = Array();
	for (var i = 0; i<8;i++){
		$("#"+xiId+"_"+i).val("");
	}
	for (var i = 1; i<xVals.length;i++){
		xi[i] = parseFloat(((xVals[i]+xVals[i-1])/2).toFixed(5));
		$("#"+xiId+"_"+i).val (xi[i]);
	}
//	console.log (xi);
	return xi;
}
function getFXi(xi,f, fxiId){
	fXi = Array();
	for (var i = 0; i<8;i++){
		$("#"+fxiId+"_"+i).val("");
	}
	for (var i = 1; i<xi.length;i++){
		x = xi[i];
		with (Math){
			fXi[i] = eval(f);
			$("#"+fxiId+"_"+i).val (fXi[i]);
		}
	}
	return fXi;
}

function verboseRechteckRegel(n,a,b,fxi){
	var verbose = "<h2>Rechteckregel für $n="+n+"$ (äquidistantes Intervall)</h2>";
	$("#solution").append(verbose);
	verbose = "";
	verbose +="$\\int_{"+a+"}^{"+b+"} f(x) dx " +
			"\\approx R_f " +
			"= \\sum_{i=1}^n f(\\xi_i)(x_i-x_{i-1}) " +
			"= \\sum_{i=1}^"+n+"f(\\xi_i) \\frac{" +(b-a)+"}{"+n+"}$<br/>" +
			"$\\qquad =  \\frac{" +(b-a)+"}{"+n+"}[";
	var sum = 0;
	for (var i = 1; i<n+1;i++){
		sum += fxi[i];
		verbose += "("+parseFloat(fxi[i].toFixed(5))+")";
		if (i<n-1){
			verbose +="+";
		}
	}
	verbose +=	"]=" +
			parseFloat(((b-a)*sum/n).toFixed(5)) +
			"$";
	$("#solution").append(verbose);
}



function getRechteckRegel(){

	$(".rechteckregel").css("visibility","visible");
	$(".quadraturformel").css("visibility","hidden");
	$(".left-black").css("borderLeft","none");
	
	var x;
	var a;
	var b;
	startVal = "a";
	endVal = "b";
	if ($("#"+startVal).val()!=""){
		a = parseFloat($("#"+startVal).val());
	} else {
		a = 0;
		$("#"+startVal).val(a);
	}
	if ($("#"+endVal).val()!=""){
		b = parseFloat ($("#"+endVal).val());
	} else {
		b = 1;
		$("#"+endVal).val(1);
	}
	
	x = Array();
	for (var i = 0; i<8;i++){
		if ($("#x_"+i).val ()!=""){
			x[i] = parseFloat($("#x_"+i).val ());
		};
	}
	n = x.length-1;
	$("#n").val(n);
	h = (b-a)/n;
	showRechteckRegelTheory();
	xi = getXiFromX(x,"xi");
	fxi = getFXi(xi,$('#f').val(),"fxi");
	verboseRechteckRegel(n,a,b, fxi);
	
	
	retranslate();
}