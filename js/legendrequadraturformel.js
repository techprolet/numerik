/**
 * "Numerik Klausur"
 * Numerische Integration, Gauss-Legendre Quadraturformel
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */

legendreNST = Array();
legendreNST[1] = Array();
legendreNST[1].x = [0];
legendreNST[1].w = [2];
legendreNST[1].xVerbose = ["0"];
legendreNST[1].wVerbose = ["2"];
legendreNST[2] = Array();
legendreNST[2].x = [-Math.sqrt(1/3),Math.sqrt(1/3)];
legendreNST[2].w = [1,1];
legendreNST[2].xVerbose = ["-\\sqrt{\\frac{1}{3}}","\\sqrt{\\frac{1}{3}}"];
legendreNST[2].wVerbose = ["1","1"];
legendreNST[3] = Array();
legendreNST[3].x = [-Math.sqrt(3/5),0,Math.sqrt(3/5)];
legendreNST[3].w = [5/9,8/9,5/9];
legendreNST[3].xVerbose = ["-\\sqrt{\\frac{3}{5}}","0","\\sqrt{\\frac{3}{5}}"];
legendreNST[3].wVerbose = ["\\frac{5}{9}","\\frac{8}{9}","\\frac{5}{9}"];
legendreNST[4] = Array();
legendreNST[4].x = [-Math.sqrt((3+(2*Math.sqrt(6/5)))/7),-Math.sqrt((3-(2*Math.sqrt(6/5)))/7),Math.sqrt((3-(2*Math.sqrt(6/5)))/7),Math.sqrt((3+(2*Math.sqrt(6/5)))/7)];
legendreNST[4].w = [(18-Math.sqrt(30))/36,(18+Math.sqrt(30))/36,(18+Math.sqrt(30))/36,(18-Math.sqrt(30))/36];
legendreNST[4].xVerbose = ["-\\sqrt{\\frac{3+2\\sqrt{\\frac{6}{5}}}{7}}","-\\sqrt{\\frac{3-2\\sqrt{\\frac{6}{5}}}{7}}","\\sqrt{\\frac{3-2\\sqrt{\\frac{6}{5}}}{7}}","\\sqrt{\\frac{3+2\\sqrt{\\frac{6}{5}}}{7}}",];
legendreNST[4].wVerbose = ["\\frac{18-\\sqrt{30}}{36}","\\frac{18+\\sqrt{30}}{36}","\\frac{18+\\sqrt{30}}{36}","\\frac{18-\\sqrt{30}}{36}",];

gaussIntegral = Array();
gaussIntegral[0] = "2";
gaussIntegral[1] = "0";
gaussIntegral[2] = "\\frac{2}{3}";
gaussIntegral[3] = "0";
gaussIntegral[4] = "\\frac{2}{5}";

orthogonalP = Array();
orthogonalP[0] = "1";
orthogonalP[1] = "x";
orthogonalP[2] = "\\frac{1}{2}(3x^2 - 1) \\Rightarrow$ NST für $x^2-\\frac{1}{3}$ finden $ ";
orthogonalP[3] = "\\frac{1}{2}(5x^3 - 3x) \\Rightarrow$ NST für $x^3-\\frac{3}{5}x$ finden $ ";
orthogonalP[4] = "\\frac{1}{8}(35x^4-30x^2+3)\\Rightarrow$ NST für $x^4-\\frac{6}{7}x^2+\\frac{3}{35}$ finden $ ";
orthogonalP[5] = "\\frac{1}{8}(63x^5-70x^3+15x)\\Rightarrow$ NST für $x^5-\\frac{10}{9}x^3+\\frac{5}{21}$ finden $ ";




function showLegendreQuadraturformelTheory(){
	$("#types").html("" +
			"<h3>Gauß Quadraturformel</h3>" +
			"<p>" +
			"\\[\\int_{-1}^{1} f(x) dx  = \\int_{-1}^{1} \\omega(x) g(x) dx \\approx Q_n = \\sum_{i=0}^n \\omega_i'g(x_i) A_i	\\]" +
			"<br/>" +
			"<h4>Bekannte Gewichtsfunktionen:</h4>" +
			"<table>" +
			"<thead>" +
			"<tr><td><b>Intervall</b></td><td><b> $\\omega(x)$</b></td><td><b>Orthogonale Polynomen</b></td></tr>\n" +
			"</thead>" +
			"<tbody>" +
			"<tr><td>$[-1,1]$ </td><td> $\\omega(x) = 1$ </td><td> Legendre Polynom</td></tr>\n" +
			"<tr><td>$(-1,1)$ </td><td> $\\omega(x) = \\frac{1}{\\sqrt{1-x^2}}$ </td><td> Tschebyscheff Polynom</td></tr>\n" +
			"<tr><td>$[-1,1]$ </td><td> $\\omega(x) = \\sqrt{1-x^2}$ </td><td> Tschebyscheff Polynom (2. Sorte)</td></tr>\n" +
			"<tr><td>$(-\\infty,\\infty)$ </td><td> $\\omega(x) = e^{-x^2}$ </td><td> Hermite Polynom</td></tr>\n" +
			"</tbody>" +
			"</table>" +
			"<h4>Intervalltrasformation:</h4>" +
			"\\[ [-1,1] \\leftrightarrow [a,b]\\]" +
			"\\[" +
			"\\left." +
			"\\begin{array}\\\\" +
			"x = x(u) = \\frac{b-a}{2}u + \\frac{b+a}{2}\\\\" +
			"f(x) \\longrightarrow g(u) = f(\\frac{b-a}{2}u+\\frac{b+a}{2})" +
			"\\end{array}" +
			"\\right.\\qquad\\longrightarrow\\qquad" +
			"\\int_a^b f(x) dx = \\frac{b-a}{2} \\int_{-1}^{1}f (\\frac{b-a}{2} u + \\frac{b+a}{2})du\\]" +
			"" +
			"" +
			"</p>");
	retranslate();
}



function verboseLegendreQuadraturformel(f,a,b,n){
	var verbose = "<h2>Legendre-Quadraturformel für $n="+n+"$</h2>";
	verbose += "<b>$n="+n+"$, Stützstellen: $n+1="+(n+1)+"$, Exaktheitsgrad: $2n+1="+(2*n+1)+"$</b><br/>";
	$("#solution").append(verbose);
	
	verbose = "";

	for (var i = 0;i<n+1;i++){
		$("#qfx_"+i).html("$x_"+i+"="+legendreNST[n+1].xVerbose[i]+"$<br/>$="+parseFloat(legendreNST[n+1].x[i].toFixed(5))+"$");
		$("#qfy_"+i).html("$w_"+i+"="+legendreNST[n+1].wVerbose[i]+"$"+(n==1?"":"<br/>$="+parseFloat(legendreNST[n+1].w[i].toFixed(5))+"$"));

	}

	verbose+="<br/>" +
			"$P_"+(n+1)+"="+orthogonalP[n+1]+"$<br/><br/>";
	
	
	verbose+="&nbsp;$\\begin{array}{lllllllll}\\\\";
	for (var i = 0;i<n+1;i++){
		for (var k = 0; k<n+1;k++){
			verbose+= "" +(k>0?"+ & ":" & ")+
					"a_"+k+"" + (i>0?("x_"+k+(i>1?"^"+i:"")):"")+
					"&" +
					""
					;	
		}
		verbose+= "=" +
				"\\int_{-1}^{1}" +(i>0?"x"+(i>1?"^"+i:""):"1")+"dx" +
						"& ="+ gaussIntegral[i]+
						"\\\\\n";
	}
	verbose+="\\end{array}" +
	"\\right\\}" +
	"" +
	"" +
	"$";
	
	verbose+="&nbsp;$" +
			"" +
	"\\Rightarrow \\begin{array}{lllllllll}\\\\";
	for (var i = 0;i<n+1;i++){
		for (var k = 0; k<n+1;k++){
			verbose+= "" +(k>0?"+ & ":" & ")+
					(i>0?("("+legendreNST[n+1].xVerbose[k]+")"+(i>1?"^"+i:"")):"")+
					"a_"+k+"" + 
					"&" +
					""
					;	
		}
		verbose+= "" +
						"& ="+ gaussIntegral[i]+
						"\\\\\n";
	}
	verbose+="\\end{array}" +
	"\\right\\}" +
	"" +
	"" +
	"$&nbsp;";
	
	verbose+="&nbsp;$" +
		"" +
	"\\Rightarrow \\begin{array}{lllllllll}\\\\";
	for (var i = 0;i<n+1;i++){
		verbose+= "" +
				"a_"+i+"" + 
				"&" +
				"= &"+
				legendreNST[n+1].wVerbose[i]+
				"= &"+
				parseFloat(legendreNST[n+1].w[i].toFixed(5))+
					"\\\\\n";
	}
	verbose+="\\end{array}" +
	"" +
	"" +
	"$&nbsp;<br/>";

	
	verbose+="<h4>Quadraturformel auf $[-1,1]$:</h4>";
	verbose+="$\\tilde{Q}_"+(2*n+1) +
			"(f)\\quad=";
	for (var i = 0; i<n+1;i++){
		verbose += legendreNST[n+1].wVerbose[i]+
				"\\cdot f(" +
				 legendreNST[n+1].xVerbose[i]+
				")"+(i<n?"+\\quad":"");
	}
	verbose += "=$<br/>$\\quad = ";
	for (var i = 0; i<n+1;i++){
		verbose += parseFloat(legendreNST[n+1].w[i].toFixed(5))+
				"\\cdot f(" +
				parseFloat(legendreNST[n+1].x[i].toFixed(5))+
				")"+(i<n?"+\\quad":"\\qquad");
	}
	verbose+="$<br/>";
	
	verbose+="<h4>Quadraturformel auf $[a,b]: ["+a+","+b+"]$:</h4>";
	verbose+="$Q_"+(2*n+1) +
			"(f)=$<br/>" +
			"$=\\frac{(b-a)}{2}" +
					"\\left[";
	for (var i = 0; i<n+1;i++){
		verbose += legendreNST[n+1].wVerbose[i]+
				"\\cdot f\\left(" +
				"\\frac{(b-a)}{2}(" +
				 legendreNST[n+1].xVerbose[i]+
				")+" +
				"\\frac{(b+a)}{2}" +
						"\\right)" +
						""+(i<n?"\\quad+\\quad":"");
	}
	verbose += "\\right]" +
			"" +
			"= $<br/>";
	
	verbose+="$" +
			"=\\frac{"+b+(a!=0?"-("+a+")":"")+"}{2}" +
					"\\left[";
	for (var i = 0; i<n+1;i++){
		verbose += legendreNST[n+1].wVerbose[i]+
				"\\cdot f\\left(" +
				"\\frac{"+b+(a!=0?"-("+a+")":"")+"}{2}(" +
				 legendreNST[n+1].xVerbose[i]+
				")+" +
				"\\frac{"+b+(a!=0?"+("+a+")":"")+"}{2}" +
						"\\right)" +
						""+(i<n?"\\quad+\\quad":"");
	}
	verbose += "\\right] = " +
			"$<br/>" +
			"$= " +
			parseFloat(((b-a)/2.0).toFixed(5))+"\\left[";
	for (var i = 0; i<n+1;i++){
		verbose += parseFloat(legendreNST[n+1].w[i].toFixed(5))+
				"\\cdot f(" +
				parseFloat((((b-a)/2.0)*legendreNST[n+1].x[i]+((b+a)/2.0)).toFixed(5))+
				")"+(i<n?"\\quad+\\quad":"");
	}
	verbose+="\\right]$<br/>";
	verbose+="$=";
	var q =0;
	for (var i = 0; i<n+1;i++){
		with (Math){
			x = (((b-a)/2.0)*legendreNST[n+1].x[i]+((b+a)/2.0));
			q+=legendreNST[n+1].w[i] * eval(f);
		}
	}
	q*=((b-a)/2.0);
	verbose+=q;
	verbose+="$<br/>" +
			"" +
			"<b>f(x)="+f+"</b>";
	
	

	$("#solution").append(verbose);
	return q;
}



function getLegendreQuadraturformel(){

	$(".rechteckregel").css("visibility","hidden");
	$(".quadraturformel").css("visibility","visible");
	$(".left-black").css("borderLeft","2px solid black");
	for (var i = 0;i<7;i++){
		$("#qfx_"+i+",#qfy_"+i).html("");
	}
	
	var n = parseInt($("#qf_n").val());
//	getValuesFromFunction($('#f').val(),(n+2),'a','b','x','y');
	var x;
	var a;
	var b;
	startVal = "a";
	endVal = "b";
	if ($("#"+startVal).val()!=""){
		a = parseFloat($("#"+startVal).val());
	} else {
		a = 0;
		$("#"+startVal).val(a);
	}
	if ($("#"+endVal).val()!=""){
		b = parseFloat ($("#"+endVal).val());
	} else {
		b = 1;
		$("#"+endVal).val(1);
	}

//	x = Array();
//	for (var i = 0; i<n+1;i++){
//		if ($("#x_"+(i)).val ()!=""){
//			x[i] = parseFloat($("#x_"+(i+1)).val ());
//		} else {
//			break;
//		};
//	}

//	y = Array();
//	for (var i = 0; i<n+1;i++){
//		y[i] = parseFloat($("#y_"+(i+1)).val ());
//	}
//	$("#n").val((n+2));
	
//	h = (x[n]-x[0])/ (n);
//	$("#h").html(parseFloat(h.toFixed(5)));
	
	f = $('#f').val();
	
	showLegendreQuadraturformelTheory();

	verboseLegendreQuadraturformel(f,a,b, n);
	
	
	retranslate();
}