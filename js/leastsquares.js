/**
 * "Numerik Klausur"
 * Minimum error squares approximation method.
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */



function showLeastSquaresTheory(){
	$("#types").html("<p>" +
			"\\underline{Gegeben:} $(x_i,y_i),\\quad i=1,...,n\\quad (x_i\\neq x_j$ für $i\\neq j$ nicht zwingend vorausgesetzt)<br/>" +
			"Ansatzfunktionen: $\\varphi_j(x),\\quad j=1,...,m\\quad m\\neq n$ möglich (i.a. $n>>m$)<br/>" +
			"Bilden Linearkombination $f_{a_1...a_m}(x)=\\sum_{j=1}^{m}a_j\\varphi_j(x)$<br/>" +
			"\\underline{Gesucht:} $a_1,...,a_m$:" +
			"\\[\\sum_{i=1}^n |f_{a_1a_2...a_m}(x_i)-y_i|^2{ }_{\\overrightarrow{a_1,...,a_m}}\\min\\]" +
			"Wir definieren $F(a_1,...,a_m)=\\sum_{i=1}^n (\\sum_{j=1}^m a_j\\varphi_j(x)-y_i)^2$<br/>" +
			"Notwendige Bedingung für Extremum $\\frac{\\partial F}{\\partial a_k}=0,\\quad k=1,...,m$<br/>" +
			"$\\frac{\\partial F}{\\partial a_k}=\\frac{\\partial }{\\partial a_k}\\sum_{i=1}^n (\\sum_{j=1}^m a_j\\varphi_j(x_i)-y_i)^2=\\sum_{i=1}^n 2(\\sum_{j=1}^m a_j\\varphi_j(x_i)-y_i)\\varphi_k(x_i))=0$<br/>" +
			"$\\Rightarrow \\sum_{i=1}^n \\sum_{j=1}^m\\varphi_j(x_i)\\varphi_k(x_i)a_j=\\sum_{i=1}^ny_i\\varphi_k(x_i),\\quad k=1,...,m$<br/>" +
			"Wir definieren $A=(a_{ij})_{i=1,j=1}^{n,m}$ mit $a_{i,j}=\\varphi_j(x_i)$<br/>" +
			"$a=(a_j)_{j=1}^{m},\\quad y=(y_i)_{i=1}^n$<br/>" +
			"$\\sum_{i=1}^{n}y_i\\varphi_k(x_i)=\\sum_{i=1}^{n}\\varphi_k(x_i)y_i=(A^Ty)_k$<br/>" +
			"$\\sum_{j=1}^{m}\\sum_{i=1}^{n}\\varphi_j(x_i)\\varphi_k(x_i)a_j=" +
			"\\sum_{j=1}^{m}\\sum_{i=1}^{n}a_{ij}a_{ik}a_j =" +
			"\\sum_{i=1}^n a_{ik}(\\sum_{j=1}^m a_{ij}a_j)=" +
			"\\sum_{i=1}^n a_{ik}(Aa)_i = \\sum_{i=1}^n(A^T)_{k,i}(Aa)_i=(A^TAa)_k$<br/>" +
			"<u>Insgesamt:</u>" +
			"\\[(A^TA)a=A^Ty\\ lineares Gleichungssystem\\]" +
			"$A_{(n,m)},\\quad A^T_{(m,n)},\\quad A^TA_{(m,m)}$ (quadratisches Gleichungssystem), $(A^Ty)_{(m,1)}$<br/>" +
			"$(A^TA)^T = A^TA$ mit symmetrischer Matrix<br/>" +
			"<u>Frage:</u> $\\det(A^TA)\\neq 0$<br/>" +
			"<u>Bemerkung:</u><br/>" +
			"$F(a_1,...,a_m)$ läßt sich umschreiben als: $F(a_1,...,a_m)=||Aa-y||_{R^n}^2$" +
			"($\\Rightarrow \\min$)<br/>" +
			"<h4>lineare Regression:</h4> $\\varphi_1(x)\\equiv 1,\\quad \\varphi_2(x)=x$<br/>" +
			"$A=\\left(\\varphi_j(x_i)\\right)_{i=1,j=1}^{n,2}=\\left(\\begin{array}{ll}" +
			"1	&	x_1\\\\" +
			"1	&	x_2\\\\" +
			"1	&	x_3\\\\" +
			"...\\\\" +
			"1	&	x_n\\\\" +
			"\\end{array}\\right)$, <br/>" +
			"$A^T=\\left(\\begin{array}{lllll}" +
			"1&1&1&...&1\\\\" +
			"x_1&x_2&x_3&...&x_n\\\\" +
			"\\end{array}\\right)$,<br/>" +
			"$A^TA=\\left(\\begin{array}{ll}" +
			"n		&		\\sum_{i=1}^n x_i\\\\" +
			"\\sum_{i=1}^n x_i	&	\\sum_{i=1}^n x_i^2\\\\" +
			"\\end{array}\\right)$<br/>" +
			"zu lösen ist:<br/>" +
			"$\\left(\\begin{array}{ll}" +
			"n	&	\\sum_{i=1}^n x_i\\\\" +
			"\\sum_{i=1}^n x_i	&	\\sum_{i=1}^n x_i^2\\\\" +
			"\\end{array}\\right)" +
			"\\left(\\begin{array}{l}" +
			"a_1\\\\" +
			"a_2\\\\" +
			"\\end{array}\\right)=" +
			"\\left(\\begin{array}{l}" +
			"\\sum_{i=1}^n y_i\\\\" +
			"\\sum_{i=1}^n x_i y_i" +
			"\\end{array}\\right)$<br/>" +
			"$\\det(A^TA) = n\\sum_{i=1}^n x_i^2-(\\sum_{i=1}^n x_i)^2 =$<br/>" +
			"$n\\sum_{i=1}^n x_i^2-(\\sum_{i=1}^n x_i)(\\sum_{j=1}^n x_j)=$<br/>" +
			"$n\\sum_{i=1}^n x_i^2-\\sum_{i=1}^n \\sum_{j=1}^n x_i x_j$<br/>" +
			"benutzen: $(x_i-x_j)^2\\geq 0\\Rightarrow$<br/>" +
			"$x_i x_j\\leq \\frac{1}{2}(x_i^2 + x_j^2) \\Rightarrow" +
			"-x_i x_j = -\\frac{1}{2}(x_i^2 + x_j^2)$<br/>" +
			"$\\det(A^tA)\\geq n\\sum_{i=1}^nx_i^2 -\\underbrace{ \\frac{1}{2}\\sum_{i=1}^n \\sum_{j=1}^n (x_i^2 + x_j^2)}_{n\\sum x_i^2} =0$<br/>" +
			"$\\det(A^TA)>0$, falls nicht alle $x_i=x_1$ sind." +
			"</p>");
	retranslate();
}


function getLeastSquaresF(points,fString){
	f = Array();
	for (var i = 0; i<points.length;i++){
		with (Math){
			f[i] = eval(fString);
		}
	}
	return f;
}

function getLeastSquaresLeftSums (points,f){
	leftSums = [[0,0],[0,0]];
	for (var i = 0;i<points.length;i++){
		leftSums[0][0] += Math.pow(f[0][i],2);
		leftSums[0][1] += f[0][i]* f[1][i];
		leftSums[1][1] += Math.pow(f[1][i],2);
	}
	leftSums[1][0] = leftSums[0][1];
//	console.log("Left Sums:",leftSums);
	return leftSums;
}
function getLeastSquaresRightSums (points,f){
	rightSums = [0,0];
	for (var i = 0;i<points.length;i++){
		rightSums[0] += f[0][i]* points[i].y;
		rightSums[1] += f[1][i]* points[i].y;
	}
//	console.log("Right Sums:",rightSums);
	return rightSums;
}

function verboseLeastSquareTables(points,f){
	var verbose = "";
	verbose+= "$A = (\\phi_j(x_i))_{i=1,j=1}^{"+points.length+",2}=" +
			"\\left(" +
			"\\begin{array} \\\\";
	for (var i = 0; i<points.length;i++){
		verbose+=parseFloat(f[0][i].toFixed(5))+" & "+parseFloat(f[1][i].toFixed(5))+" \\\\";
	}
	verbose+="\\end{array}" +
			"\\right)$<br/>";
	

	verbose+= "$A^T=" +
			"\\left(" +
			"\\begin{array} \\\\";
	for (var i = 0; i<points.length;i++){
		verbose+=parseFloat(f[0][i].toFixed(5))+" & ";
	}
	verbose+="\\\\";
	for (var i = 0; i<points.length;i++){
		verbose+=parseFloat(f[1][i].toFixed(5))+" & ";
	}
	verbose+="\\end{array}" +
			"\\right)$<br/>";
	
	
	verbose+="$A^TA=\\left(\\begin{array}{ll}" +
	parseFloat(leftSums[0][0].toFixed(5))+"		&		"+parseFloat(leftSums[0][1].toFixed(5))+"\\\\" +
	parseFloat(leftSums[1][0].toFixed(5))+"	&	"+parseFloat(leftSums[1][1].toFixed(5))+"\\\\" +
	"\\end{array}\\right)$<br/> ";

	
	verbose+="$A^Ty=\\left(\\begin{array}{ll}" +
	parseFloat(rightSums[0].toFixed(5))+"\\\\" +
	parseFloat(rightSums[1].toFixed(5))+"\\\\" +
	"\\end{array}\\right)$<br/><br/> ";
	
	verbose+="$\\left(" +
			"A^TA\\right)a = A^Ty\\Rightarrow$<br/>";

	m = Array();
	for (var i = 0;i<leftSums.length;i++){
		m[i] = Array();
		for (var k = 0; k<leftSums.length;k++){
			m[i][k] = leftSums[i][k];
		}
		m[i][leftSums.length] = rightSums[i];
	}
//	console.log(m);

	$("#solution").append(verbose);
	verboseSolveMatrix (m, "a");
}

function getLeastSquares(){
	fillGivenArrays();
	showLeastSquaresTheory();
	var f_1 = $("#f_1").val().replace("x","(points[i].x)");
	var f_2 = $("#f_2").val().replace("x","(points[i].x)");
	var f = Array();
	f = [ getLeastSquaresF(points, f_1),	getLeastSquaresF(points, f_2)];
	$("#solution").html("");
	$("#solution").append("$\\phi_1 = "+$("#f_1").val()+"$<br/>");
	$("#solution").append("$\\phi_2 = "+$("#f_2").val()+"$<br/>");
	leftSums= getLeastSquaresLeftSums(points,f);
	rightSums= getLeastSquaresRightSums(points,f);
	verboseLeastSquareTables(points,f,leftSums,rightSums);
	
	

	retranslate();
};