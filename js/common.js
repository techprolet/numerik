/**
 * "Numerik Klausur"
 * Common methods.
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * 
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */


Pi = Math.PI;
e = Math.E;
numVals = 8;

var points;

function retranslate(){
	AMtranslated = false;
	translate();
}

function fillGivenArrays(){
	points = new Array();
	for (var i =0;i<numVals;i++){
		if ($("#x_"+i).val()!=""){
			points[i] = new Array();
			points[i].x=parseFloat($("#x_"+i).val());
			points[i].y=parseFloat($("#y_"+i).val());
		} else {
			break;
		}
	}
	console.log ("points size:"+points.length);
};

//initialize array
function zeros(n) {
   var array = new Array(n);
   for (var i=n; i--;) {
     array[i] = 0;
   }
   return array;
}



function lcm(a, b){
    return (a * b / gcd(a, b));
}

function gcd(a, b){
	var i = 0;
	//the following is done to improve numerical stability for floats
	while (i<5 &&((a-Math.floor(a)!=0)||(b-Math.floor(b)!=0))){
		i++;
		a*=10;
		b*=10;
	}
	console.log(i, a, b);
    // Euclidean algorithm
    var t;
    while (b != 0){
        t = b;
        b = a % b;
        a = t;
    }
    for (var k=0;k<i;k++){
    	a/=10;
    }
    return a;
}


function multiplyPolynomials(a,b){
	console.log (a);
	console.log (b);
	var c = zeros (a.length+b.length-1)
	for (var i = 0;i<b.length;i++){
		for (var k = 0;k<a.length;k++){
			c[k+i] += a[k]*b[i];
		}
	}
	a = c;
	return a;
}

function addPolynomials (a,b){
	c = a.length>=b.length?a:b
	for (var i =0;i<Math.min(a.length,b.length);i++){
		c[i] = a[i]+b[i]
	}
	return c;
}



//a = [3,1]
//b = [2,1]
//alert (addPolynomials(a, b));



function verbosePolynomial(p, withDollar = false){
	var verbose = "";
	if (withDollar) verbose += "$";
	firstAppeared = false;
	for (var i =p.length-1;i>=0;i--){
		if (firstAppeared&&(p[i]>0)){
			verbose+="+";
		}
		if (parseFloat(p[i].toFixed(5))!=0){
			if ((p[i]==-1)&&(i!=0)){
				verbose+="-";
			} else if ((p[i]!=1)||(i==0)){
				verbose+=parseFloat(p[i].toFixed(5));
			}
			firstAppeared = true;
			if (i>0){
				verbose+="x";
				if (i>1){
					verbose+="^"+i;
				}
			}
		}
	}
	if (!firstAppeared){
		verbose+="0";
	}
	if (withDollar) verbose += "$";
	return verbose;
}



function getCoefficients(factors){
	coefficients = zeros(factors.length+1);
	coefficients[0] = factors[0];
	coefficients[1] = 1;
//	console.log ("Coefficients:"+coefficients);
	for (var i = 1; i<factors.length;i++){
		var fromX = zeros(factors.length + 1);
		for (var k = 0;k<factors.length;k++){
			if (coefficients[k]!=0){
				fromX[k+1] = coefficients[k];
			}
		}
		for (var k = 0;k<factors.length;k++){
			coefficients[k]*=factors[i];
			coefficients[k]+=fromX[k];
		}
		coefficients[factors.length] = fromX[factors.length];
//		console.log ("fromX:"+fromX);
//		console.log ("Coefficients:"+coefficients);
	}
//	console.log ("Coefficients:"+coefficients);
	return coefficients;
}


function computeForX(polynomial, x){
	var y = 0;
	for (var i=0;i<polynomial.length;i++){
		y+=polynomial[i]*Math.pow(x,i);
	}
	console.log("y="+y);
	return parseFloat(y.toFixed(5));
}


function getValuesFromFunction(f,n,startVal, endVal, xId, yId){
	
	for (var i = 0;i<7;i++){
		$("#qfx_"+i+",#qfy_"+i).html("");
	}
	
	
	
	n = parseInt(n);
	$("#qf_n").val((n-2));
	var a;
	var b;
	
	if ($("#"+startVal).val()!=""){
		a = parseFloat($("#"+startVal).val());
	} else {
		a = 0;
		$("#"+startVal).val(a);
	}
	if ($("#"+endVal).val()!=""){
		b = parseFloat ($("#"+endVal).val());
	} else {
		b = 1;
		$("#"+endVal).val(1);
	}
	totalDist = b-a;
	h = totalDist/n;
	$("#h").html(parseFloat(h.toFixed(5)));
	for (var i = 0; i<8; i++){
		$("#"+xId+"_"+i).val ("");
		$("#"+yId+"_"+i).val ("");
	}
	for (var i = 0; i<(n+1); i++){
		x = totalDist * i /n + a;
		$("#"+xId+"_"+i).val (parseFloat(x.toFixed(5)));
		with (Math){
			y = eval (f);
			$("#"+yId+"_"+i).val (parseFloat(y.toFixed(5)));
		}
	}
	
}


function getEquationMatrixString(m, xName = "x"){
	var n = m.length;
	var verbose = "";
	verbose +=" &nbsp; $\\left(" +
			"\\begin{array}\\\\";
	for (var i = 0; i<n;i++){
		for (var k = 0; k<n;k++){
			verbose += parseFloat(m[i][k].toFixed(5));
			if (k<(n-1)){
				verbose+=" & ";
			} else {
				verbose+="\\\\";
			}
		}
	}

	verbose +="\\end{array}" +
			"\\right)" +
			"\\cdot" +
			"\\left(" +
			"\\begin{array}\\\\";
	for (var i = 0; i<n;i++){
				verbose+="a_"+i+"\\\\";
			}
	verbose +="\\end{array}" +
			"\\right)" +
			"=" +
			"\\left(" +
			"\\begin{array}\\\\";
	for (var i = 0; i<n;i++){
		verbose += parseFloat(m[i][n].toFixed(5));
		verbose+="\\\\";
	}
	verbose +="\\end{array}" +
			"\\right)$ &nbsp; "; 
	return verbose;
}

function verboseSolveMatrix(m, xName = "x"){
	var verbose = ""; 
	var n = m.length;
	verbose +=getEquationMatrixString(m,xName);
	verbose +=" $\\Rightarrow$ <br/>";
	var x = Array();
	for (var i = 0; i<n-1;i++){
		verbose += "<span class=\"redop\">$\\left[" +
				"\\begin{array} \\\\";
		for (var k = i+1; k<n;k++){
			var multiplier = m[k][i]/m[i][i];
			
			for (var l = i+1;l<(n+1);l++){
				m[k][l] -= m[k][i]*m[i][l]/m[i][i];
			}
				
			m[k][i] = 0;

			verbose += "m_"+k+" = m_"+k+" - "+parseFloat(multiplier.toFixed(5))+"\\cdot m_"+i+" \\\\\n";
		}
		verbose += "\\end{array}" +
				"\\right] : $ </span>";
		verbose +=getEquationMatrixString(m,xName);
		verbose +=" $\\Rightarrow$ <br/>";
	}
	
	
	for (var i = n-1; i>0;i--){
		verbose += "<span class=\"redop\">$\\left[" +
				"\\begin{array} \\\\";
		for (var k = i-1; k>=0;k--){
			var multiplier = m[k][i]/m[i][i];
			
			for (var l = i-1;l>=0;l--){
				m[k][l] -= m[k][i]*m[i][l]/m[i][i];
			}
			m[k][n] -= m[k][i]*m[i][n]/m[i][i];
			m[k][i] = 0;

			verbose += "m_"+k+" = m_"+k+" - "+parseFloat(multiplier.toFixed(5))+"\\cdot m_"+i+" \\\\\n";
		}
		verbose += "\\end{array}" +
				"\\right] : $ </span>";
		verbose +=getEquationMatrixString(m,xName);
		verbose +=" $\\Rightarrow$ <br/>";
	}
	
	


	verbose += "<span class=\"redop\">$\\left[" +
			"\\begin{array} \\\\";
	for (var i = 0; i<n;i++){
		if (m[i][i]!=1){
			var multiplier = 1.0/m[i][i];
			m[i][n] /=m[i][i];
			m[i][i] = 1;
			verbose += "m_"+i+" = m_"+i+" * "+parseFloat(multiplier.toFixed(5))+" \\\\\n";
		}
		x[i] = m[i][n];
	}
	verbose += "\\end{array}" +
		"\\right] : $ </span>";
	verbose +=getEquationMatrixString(m,xName);
	verbose +=" $ \\Rightarrow" +
			"" +
			"\\begin{array}\\\\";
	for (var i = 0;i<x.length;i++){
		verbose+= xName+"_"+i+" = "+parseFloat(x[i].toFixed(5))+"\\\\\n"; 
	}
	verbose +="\\end{array}$<br/>";
	$("#solution").append(verbose);
	
	return x;
}



