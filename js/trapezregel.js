/**
 * "Numerik Klausur"
 * Numerische Integration, Rechteckregel
 * Copyright 2014 Pavlos Iliopoulos, techprolet.com
 * This file is part of "Numerik Klausur".

	"Numerik Klausur" is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    "Numerik Klausur" is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Numerik Klausur".  If not, see <http://www.gnu.org/licenses/>.
 */


function showTrapezRegelTheory(){
	$("#types").html("" +
			"<h3>Trapezregel</h3>" +
			"<p>" +
			"$T_f = \\frac{f(a)+f(b)}{2}(b-a)$<br/>" +
			"$h = \\frac{b-a}{n}$<br/>" +
			"$T_f = \\frac{f(x_0)+f(x_1)}{2}h +\\frac{f(x_1)+f(x_2)}{2}h +...+\\frac{f(x_{n-2})+f(x_{n-1})}{2}h +\\frac{f(x_{n-1})+f(x_n)}{2}h$<br/>" +
			"$= h\\left[\\frac{1}{2}\\left(f(x_0)+f(x_n)\\right)+f(x_1)+...+f(x_{n-1})\\right]$<br/>" +
			"</p>");
	retranslate();
}



function verboseTrapezRegel(y,h,n,a,b){
	var verbose = "<h2>Trapezregel für $n="+n+"$ (äquidistantes Intervall)</h2>";
	$("#solution").append(verbose);
	var 
	tf = (y[0]+y[n])/2;
	verbose = "";
	verbose +="$\\int_{"+a+"}^{"+b+"} f(x) dx " +
			"\\approx T_f " +
			"= h\\left[\\frac{1}{2}\\left(f(x_0)+f(x_"+n+")\\right)";
	for (var i =1;i<n;i++){
		verbose+= "+f(x_"+i+")";
		tf+= y[i];
	}
	verbose +=")\\right]$<br/>" +
			"";

	verbose += "$= "+h+"\\left[\\frac{1}{2}\\left("+y[0]+"+("+y[n]+")\\right)";
	for (var i =1;i<n;i++){
		verbose+= "+("+y[i]+")";
	}
	verbose +=")\\right]$<br/>" +
	"";
	
	tf *=h;
	verbose += 	"$=" +
			parseFloat(tf.toFixed(5)) +
			"$<br/>";
	$("#solution").append(verbose);
}



function getTrapezRegel(){
	$(".rechteckregel").css("visibility","hidden");
	$(".quadraturformel").css("visibility","hidden");
	$(".left-black").css("borderLeft","none");
	var x;
	var a;
	var b;
	startVal = "a";
	endVal = "b";
	if ($("#"+startVal).val()!=""){
		a = parseFloat($("#"+startVal).val());
	} else {
		a = 0;
		$("#"+startVal).val(a);
	}
	if ($("#"+endVal).val()!=""){
		b = parseFloat ($("#"+endVal).val());
	} else {
		b = 1;
		$("#"+endVal).val(1);
	}

	x = Array();
	for (var i = 0; i<8;i++){
		if ($("#x_"+i).val ()!=""){
			x[i] = parseFloat($("#x_"+i).val ());
		};
	}

	y = Array();
	for (var i = 0; i<x.length;i++){
		if ($("#y_"+i).val ()!=""){
			y[i] = parseFloat($("#y_"+i).val ());
		};
	}
	n = x.length-1;
	$("#n").val(n);
	
	h = (x[x.length-1]-x[0])/ n;
	$("#h").html(h);
	
	showTrapezRegelTheory();

	verboseTrapezRegel(y,h,n,a,b);
	
	
	retranslate();
}